# BR Architect
This is simple website for fictional BR Architect company made for playing around with CSS.

## Build With
1. HTML
2. CSS

## License
MIT © [Raj Panchal](https://gitlab.com/rajpanchal)